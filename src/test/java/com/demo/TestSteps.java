package com.demo;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;

public class TestSteps {

	private WebDriver driver;
	private ExtentReports extent;
	private ExtentTest logger;

	@Before
	public void init() {
		// https://sites.google.com/a/chromium.org/chromedriver/downloads
		ClassLoader classLoader = getClass().getClassLoader();
		ChromeDriverService service = new ChromeDriverService.Builder()
				.usingDriverExecutable(new File(classLoader.getResource("chromedriver.exe").getFile()))
				.usingAnyFreePort().build();

		Map<String, Object> prefs = new HashMap<String, Object>();
		prefs.put("credentials_enable_service", false);
		prefs.put("profile.password_manager_enabled", false);

		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		options.addArguments("test-type");
		options.addArguments("disable-extensions");
		options.setExperimentalOption("prefs", prefs);
		driver = new ChromeDriver(service, options);

		extent = new ExtentReports("reports/extend-report/extentReport.html", true);
		extent.addSystemInfo("Host Name", "local").addSystemInfo("Environment", "Automation Testing")
				.addSystemInfo("User Name", "Kite");
		extent.loadConfig(new File(classLoader.getResource("extent-config.xml").getFile()));
	}

	public static String getScreenhot(WebDriver driver) {
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		String destination = "reports/extend-report/screenshots/" + dateName + ".png";
		File finalDestination = new File(destination);
		try {
			FileUtils.copyFile(source, finalDestination);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "screenshots/" + dateName + ".png";
	}

	@Given("^I run a search (.*)$")
	public void search(String target) {
		driver.get("https://www.google.com");
		driver.findElement(By.cssSelector("#lst-ib")).sendKeys(target);
		driver.findElement(By.cssSelector("#tsf > div.tsf-p > div.jsb > center > input[type=\"submit\"]:nth-child(1)"))
				.submit();

		Assert.assertTrue(true);
		logger = extent.startTest("I run a search " + target);
		logger.log(LogStatus.PASS, "Search Case Passed is passTest");
		logger.log(LogStatus.PASS, logger.addScreenCapture( getScreenhot(driver)));
	}

	@After
	public void close() {
		extent.flush();

		driver.close();
		driver.quit();
//		extent.close();
	}
}